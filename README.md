# gitlab-ci-templates

This contains templates that you can include for Gitlab CI jobs.

## How to use

In your `.gitlab-ci.yml`:

```yaml
include:
  - project: 'alpine/infra/gitlab-ci-templates'
    ref: master
    file: '/docker-image.yml'
```

If you want to exclude some architecture due to missing dependencies or other
reasons, you can do so by setting a variable like:

```yaml
variables:
  DISABLE_S390X: "true"
```

Don't forget to enable runners for you project.
