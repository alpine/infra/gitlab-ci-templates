#!/usr/bin/env python3

import sys
import yaml

filename = sys.argv[1]

try:
    with open(filename, 'r') as f:
        try:
            yaml.safe_load_all(f)
        except yaml.parser.ParserError as e:
            print("Could not parse {filename}".format(filename=filename))
            print(e.problem)
            print(e.problem_mark)
            sys.exit(1)
except FileNotFoundError:
    print("Could not find file {filename}".format(filename=filename))
    sys.exit(2)


print("Parsed {filename} succesfully".format(filename=filename))
